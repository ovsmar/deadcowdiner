{% block stylesheets %}
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Amatic+SC" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="/headerAdmin.css" rel="stylesheet">
{% endblock %}
<header>

    <a class="icon" onclick="myFunction()">&#9776;</a>
</header>
<nav id="navbar" class="nav">
    <ul>
        <li><a href="/admin-images">Notre histoire</a></li>
        <li><a href="/produits">Carte</a></li>
        <li><a href="/reservations">Reservations</a></li>
        <li><a href="/commentaire">Votre avis</a></li>
        <li><a id="logout" href="/logout">Logout</a></li>
    </ul>
</nav>