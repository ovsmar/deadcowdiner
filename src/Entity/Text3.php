<?php

namespace App\Entity;

use App\Repository\Text3Repository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: Text3Repository::class)]
class Text3
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    private $text4;

    #[ORM\Column(type: 'text')]
    private $text5;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText4(): ?string
    {
        return $this->text4;
    }

    public function setText4(string $text4): self
    {
        $this->text4 = $text4;

        return $this;
    }

    public function getText5(): ?string
    {
        return $this->text5;
    }

    public function setText5(string $text5): self
    {
        $this->text5 = $text5;

        return $this;
    }
}
