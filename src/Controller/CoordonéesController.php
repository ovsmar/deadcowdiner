<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoordonéesController extends AbstractController
{
    #[Route('/coordonees', name: 'app_coordonees')]
    public function index(): Response
    {
        return $this->render('coordonees.html.twig', [
            'controller_name' => 'CoordonéesController',
        ]);
    }
}