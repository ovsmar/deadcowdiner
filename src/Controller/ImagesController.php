<?php

namespace App\Controller;

use App\Entity\Images;
use App\Form\ImagesType;
use App\Repository\ImagesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;

#[Route('/admin-images')]
class ImagesController extends AbstractController
{
    #[Route('/', name: 'app_images_index', methods: ['GET'])]
    public function index(ImagesRepository $imagesRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        
        return $this->render('images/index.html.twig', [
            'images' => $imagesRepository->findAll(),
            'showEdit' => true
        ]);
    }

    #[Route('/new', name: 'app_images_new', methods: ['GET', 'POST'])]
    public function new(Request $request,FileUploader $fileUploader, ImagesRepository $imagesRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $image = new Images();
        $form = $this->createForm(ImagesType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imagesFile = $form->get('image1')->getData();
            if ($imagesFile) {
                $imagesFileName = $fileUploader->upload($imagesFile);
                $image->setImage1($imagesFileName);
            }
            $imagesFile = $form->get('image2')->getData();
            if ($imagesFile) {
                $imagesFileName = $fileUploader->upload($imagesFile);
                $image->setImage2($imagesFileName);
            }
            $imagesFile = $form->get('image3')->getData();
            if ($imagesFile) {
                $imagesFileName = $fileUploader->upload($imagesFile);
                $image->setImage3($imagesFileName);
            }
            $imagesFile = $form->get('image4')->getData();
            if ($imagesFile) {
                $imagesFileName = $fileUploader->upload($imagesFile);
                $image->setImage4($imagesFileName);
            }
            $imagesFile = $form->get('image5')->getData();
            if ($imagesFile) {
                $imagesFileName = $fileUploader->upload($imagesFile);
                $image->setImage5($imagesFileName);
            }
            
            
            $imagesRepository->add($image);
            return $this->redirectToRoute('app_images_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('images/new.html.twig', [
            'image' => $image,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_images_show', methods: ['GET'])]
    public function show(Images $image): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('images/show.html.twig', [
            'image' => $image,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_images_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request,FileUploader $fileUploader, Images $image, ImagesRepository $imagesRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $form = $this->createForm(ImagesType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imageidFile = $form->get('image1')->getData();
            
            if ($imageidFile) {
                $imageidFileName = $fileUploader->upload($imageidFile);
                $image->setImage1($imageidFileName);
            }
            $imageidFile = $form->get('image2')->getData();
            
            if ($imageidFile) {
                $imageidFileName = $fileUploader->upload($imageidFile);
                $image->setImage2($imageidFileName);
            }
            $imageidFile = $form->get('image3')->getData();
            
            if ($imageidFile) {
                $imageidFileName = $fileUploader->upload($imageidFile);
                $image->setImage3($imageidFileName);
            }
            $imageidFile = $form->get('image4')->getData();
            
            if ($imageidFile) {
                $imageidFileName = $fileUploader->upload($imageidFile);
                $image->setImage4($imageidFileName);
            }
            $imageidFile = $form->get('image5')->getData();
            
            if ($imageidFile) {
                $imageidFileName = $fileUploader->upload($imageidFile);
                $image->setImage5($imageidFileName);
            }

            
            $imagesRepository->add($image);
            return $this->redirectToRoute('app_images_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('images/edit.html.twig', [
            'image' => $image,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_images_delete', methods: ['POST'])]
    public function delete(Request $request, Images $image, ImagesRepository $imagesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$image->getId(), $request->request->get('_token'))) {
            $imagesRepository->remove($image);
        }

        return $this->redirectToRoute('app_images_index', [], Response::HTTP_SEE_OTHER);
    }
}