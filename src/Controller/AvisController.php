<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Form\CommentaireType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CommentaireRepository;
use Symfony\Component\HttpFoundation\Request;

class AvisController extends AbstractController
{
    #[Route('/avis', name: 'app_avis',methods:['GET', 'POST'])]
    public function index(Request $request,CommentaireRepository $commentaireRepository): Response
    
    {

        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentaireRepository->add($commentaire);
            return $this->redirectToRoute('app_avis', [], Response::HTTP_SEE_OTHER);
        }
        
        $contex= array('titre' => 'coucou' , 
        'commentaires'=> $commentaireRepository->findAll(), 
        'show'=> false,
        'form' => $form->createView()
        
    );

        return $this->render('avis.html.twig',$contex);
    }
     }