<?php

namespace App\Controller;

use App\Entity\Produits;
use App\Service\FileUploader;
use App\Form\ProduitsType;
use App\Repository\ProduitsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/produits')]
class ProduitsController extends AbstractController
{
    #[Route('/', name: 'app_produits_index', methods: ['GET'])]
    public function index(ProduitsRepository $produitsRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        
        return $this->render('produits/index.html.twig', [
            'produits' => $produitsRepository->findAll(),
            'show' => true
            
        ]);
    }

    #[Route('/new', name: 'app_produits_new', methods: ['GET', 'POST'])]
    public function new(Request $request,FileUploader $fileUploader, ProduitsRepository $produitsRepository): Response
    {
        
        $this->denyAccessUnlessGranted('ROLE_USER');
        $produit = new Produits();
        $form = $this->createForm(ProduitsType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imageidFile = $form->get('imageid')->getData();
            
            if ($imageidFile) {
                $imageidFileName = $fileUploader->upload($imageidFile);
                $produit->setImageid($imageidFileName);
            }

            
            $produitsRepository->add($produit);
            return $this->redirectToRoute('app_produits_index', [], Response::HTTP_SEE_OTHER);
        }
        // dd($form);
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->renderForm('produits/new.html.twig', [
            'produit' => $produit,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_produits_show', methods: ['GET'])]
    public function show(Produits $produit): Response
    {
        
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('produits/show.html.twig', [
            'produit' => $produit,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_produits_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request,FileUploader $FileUploader, Produits $produit, ProduitsRepository $produitsRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $form = $this->createForm(ProduitsType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imageidFile = $form->get('imageid')->getData();
            if ($imageidFile) {
                $imageidFileName = $FileUploader->upload($imageidFile);
                $produit->setImageid($imageidFileName);
            }
            
            $produitsRepository->add($produit);
            return $this->redirectToRoute('app_produits_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('produits/edit.html.twig', [
            'produit' => $produit,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_produits_delete', methods: ['POST'])]
    public function delete(Request $request, Produits $produit, ProduitsRepository $produitsRepository): Response
    
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        if ($this->isCsrfTokenValid('delete'.$produit->getId(), $request->request->get('_token'))) {
            $produitsRepository->remove($produit);
        }

        return $this->redirectToRoute('app_produits_index', [], Response::HTTP_SEE_OTHER);
    }
}