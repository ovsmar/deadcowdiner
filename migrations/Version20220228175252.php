<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220228175252 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reservations ADD nombre_de_personnes INT NOT NULL, ADD heures TIME NOT NULL, ADD date DATE NOT NULL, DROP nombres, DROP heure, CHANGE email email VARCHAR(50) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reservations ADD heure INT NOT NULL, DROP heures, DROP date, CHANGE email email VARCHAR(255) NOT NULL, CHANGE nombre_de_personnes nombres INT NOT NULL');
    }
}
