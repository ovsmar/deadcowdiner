<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220301094749 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_BE2DDF8CD05B04D2');
        $this->addSql('DROP INDEX UNIQ_BE2DDF8CD05B04D2 ON produits');
        $this->addSql('ALTER TABLE produits ADD imageid VARCHAR(255) NOT NULL, DROP imageid_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE produits ADD imageid_id INT DEFAULT NULL, DROP imageid');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_BE2DDF8CD05B04D2 FOREIGN KEY (imageid_id) REFERENCES images (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BE2DDF8CD05B04D2 ON produits (imageid_id)');
    }
}
